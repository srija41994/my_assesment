#!/bin/bash

minprice=0
maxprice=0
tobuy=()
tosell=()
runprocess=True
buyingtime=9
sellingtime=9
profit=0

stockprices=(10 8 7 9 5 7 10 11 10 8)
processstockpricing=( "${stockprices[@]}" )

# Function to get the min and max values
get_min_max() {
  min=${processstockpricing[$1]}
  max=${processstockpricing[$2]}
  #if [ -z "$min"]; then
  #  min=${processstockpricing[2]}
  #fi
  for n in ${processstockpricing[@]}
  do
    #echo "${processstockpricing[@]}"
    ((n > max)) && max=$n
    ((n < min)) && min=$n
  done
  minprice=$min
  maxprice=$max
}

# Function to get the min and max array index value
getindexvalues() {
for stock in ${!stockprices[@]}
do
  if [[ "${stockprices[$stock]}" == $2 ]]; then
    tosell+=($stock)
  elif [[ "${stockprices[$stock]}" == $1 ]]; then
    tobuy+=($stock)
  fi
done
}

# Function to get the timings
gettime() {
    bprice=${stockprices[$1]}
    sprice=${stockprices[$2]}
    profit=`expr $sprice - $bprice`
    buyingtime=`expr $1 + $buyingtime`
    sellingtime=`expr $2 + $sellingtime`
}

inc=0

# while loop to get the min and max values to calculate profit
while $runprocess ; do
  get_min_max $inc 0
  getindexvalues $minprice $maxprice
  for sell in ${tosell[@]}; do
    for buy in ${tobuy[@]}; do
      if [ $sell > $buy ] ; then
        finalsell=$sell
        finalbuy=$buy
      fi
    done
  done
  if [ $finalbuy -gt $finalsell ]; then
    #unset processstockpricing[$finalbuy]
    unset processstockpricing[$finalsell]
    #echo "${processstockpricing[@]}"
    minprice=0
    maxprice=0
    tobuy=()
    tosell=()
    inc=`expr $inc + 1`
    get_min_max $inc 0
    getindexvalues $minprice $maxprice
    if [ $finalbuy -lt $finalsell ]; then
      runprocess=False
    fi
  else
    runprocess=False
  fi
done


gettime $finalbuy $finalsell
echo "Maximum possible profit, buying at $buyingtime:00 and selling at $sellingtime:00: €$profit "


