variable "bucket_names" {
  description = "Create S3 Bucket with these names"
  type        = list(string)
  default = []
}

