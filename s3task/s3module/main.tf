resource "aws_s3_bucket" "createbucket" {
  count = length(var.bucket_names)
  bucket = var.bucket_names[count.index]
  acl    = "private"
}

